# humsize

Humanize du(1) output and other byte size columns with just AWK. This is useful on systems without `du -h` (commercial UNIX?) or `sort -h` (NetBSD 9, for which I wrote this script).

For example,

    du -sk ./* | sort -n | humsize

produces an output like

    du -sh ./* | sort -h

The `humanize` function is self-contained and reusable.

See the source code comments for the license (two-clause BSD) and more information.

## Usage

```none
humsize 0.5.0
Humanize du(1) output and other columns of file, disk, etc. sizes in bytes.

Usage: humsize [option=value ...]

Options:

The values assigned to the options below are the default values.

    field=1               What field to humanize.

    format='  %4.0f%1s'   The field format string: file size, unit prefix.

    large=%6.1f%1s        The field format for sizes at or above the threshold.

    multiplier=1024       The number by which the input field is multiplied.
How many bytes there are in one unit of input.

    threshold=1073741824  The size in bytes at which to start using the "large"
format. Set to a negative value to disable.

    total=0               Whether to print a sum total after the end of the
input. Set to a non-zero non-empty value to enable. The total is only printed
if more than one line is humanized.

    units=,K,M,G,T        The file size units in 1024 increments. Format:
"bytes,KiB,MiB,...".

    zero=''               A special string to use instead of the format string
for size zero. The default value is empty, which tells the program to use the
same formatting it uses for non-zero size.
```

## Similar programs

* [GNU `numfmt`](https://www.gnu.org/software/coreutils/manual/html_node/numfmt-invocation.html) (`gnumfmt` on NetBSD; package [sysutils/coreutils](https://cdn.netbsd.org/pub/pkgsrc/current/pkgsrc/sysutils/coreutils/index.html))
* [hum.awk](https://www.unitedbsd.com/d/1026-du-h-sort-h-alternatives-for-netbsd)
